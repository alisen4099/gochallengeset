package main

import (
	"testing"
)

func TestFindMostRepeated(t *testing.T) {
	tests := []struct {
		input  []string
		output string
	}{
		{[]string{"apple", "pie", "apple", "red", "red", "red"}, "red"},
		{[]string{"hello", "world", "hello", "go"}, "hello"},
		{[]string{"single"}, "single"},
		{[]string{}, ""},
	}

	for i, test := range tests {
		actual := findMostRepeated(test.input)
		if actual != test.output {
			t.Errorf("Test case %d: Expected %q, got %q", i, test.output, actual)
		}
	}
}
