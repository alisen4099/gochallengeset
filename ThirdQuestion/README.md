# Problem Description

Write a function which takes one parameter as an array/list.
Find most repeated data within a given array.
Test with different datasets.
Input : 
``` 
["apple","pie","apple","red","red","red"]
``` 
Output :
``` 
"red"
``` 
# Problem Solution Approach

1. I created a map to hold the count of each word.
2. Iterate over the input array and update the map.
3. Iterate over the map and find the word with the highest count.
4. Return the word with the highest count.

# How to run main file

``` 
go run main.go
```

# How to run test file

```
go test
```