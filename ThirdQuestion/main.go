package main

import (
	"fmt"
)

// findMostRepeated takes a slice of strings and returns the most repeated string
func findMostRepeated(words []string) string {
	// Initialize a map to keep track of word frequencies
	wordCount := make(map[string]int)

	// Initialize variables to keep track of the most repeated word
	var mostRepeatedWord string
	var maxCount int

	// Count the occurrences of each word and find the most repeated one
	for _, word := range words {
		wordCount[word]++

		if wordCount[word] > maxCount {
			maxCount = wordCount[word]
			mostRepeatedWord = word
		}
	}

	return mostRepeatedWord
}

func main() {
	// Test the function with different datasets
	test1 := []string{"apple", "pie", "apple", "red", "red", "red"}
	fmt.Printf("Most repeated word in test1: %s\n", findMostRepeated(test1))
}
