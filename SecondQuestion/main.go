package main

import (
	"fmt"
	"math"
)

func printSquaresAndTwo(n int) {
	// termination conditon
	if n < 2 {
		return
	}

	printSquaresAndTwo(n - 1)

	sqrt := int(math.Sqrt(float64(n)))
	// check if the given number is the square root of a number
	if sqrt*sqrt == n {
		fmt.Println(n)
	}

	if n == 2 {
		fmt.Println(n)
	}
}

func main() {
	printSquaresAndTwo(125)
}
