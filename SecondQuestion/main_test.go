package main

import (
	"bytes"
	"io"
	"os"
	"testing"
)

func captureOutput(f func()) string {
	r, w, _ := os.Pipe()
	old := os.Stdout
	os.Stdout = w

	f()

	w.Close()
	os.Stdout = old
	var buf bytes.Buffer
	io.Copy(&buf, r)
	return buf.String()
}

func TestPrintSquaresAndTwo_9(t *testing.T) {
	output := captureOutput(func() { printSquaresAndTwo(9) })
	expected := "2\n4\n9\n"
	if output != expected {
		t.Errorf("Expected %s, got %s", expected, output)
	}
}

func TestPrintSquaresAndTwo_16(t *testing.T) {
	output := captureOutput(func() { printSquaresAndTwo(16) })
	expected := "2\n4\n9\n16\n"
	if output != expected {
		t.Errorf("Expected %s, got %s", expected, output)
	}
}

func TestPrintSquaresAndTwo_25(t *testing.T) {
	output := captureOutput(func() { printSquaresAndTwo(25) })
	expected := "2\n4\n9\n16\n25\n"
	if output != expected {
		t.Errorf("Expected %s, got %s", expected, output)
	}
}

func TestPrintSquaresAndTwo_1(t *testing.T) {
	output := captureOutput(func() { printSquaresAndTwo(1) })
	expected := ""
	if output != expected {
		t.Errorf("Expected %s, got %s", expected, output)
	}
}
