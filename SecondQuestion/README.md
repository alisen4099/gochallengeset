# Problem Description

Write a recursive function which takes one integer parameter.
Please bear in mind that finding the algorithm needed to generate
the output below is the main point of the question. Please do not
ask which algorithm to use.
Input : 
``` 
9
``` 
Output :
``` 
2
4
9
``` 
# Problem Solution Approach
1. I look at the pattern and see 4 and 9 to be the square of 2 and 3 respectively. And I thought 2 is the termination condition for the recursion. 
2. Since the output is printed in increasing order, The real stack trace is 2,4,9(consider the most rigt side as the bottom).
3. So I follow this pattern and write the code.


# How to run main file

``` 
go run main.go
```

# How to run test file

```
go test
```