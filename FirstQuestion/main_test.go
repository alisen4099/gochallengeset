package main

import (
	"fmt"
	"testing"
)

func TestSortWordsByA_StandardCase(t *testing.T) {
	words := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}
	sortWordsByA(words)
	expected := []string{"aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd", "fdz", "ef", "kf", "zc", "l"}
	if fmt.Sprint(words) != fmt.Sprint(expected) {
		t.Errorf("Expected %v, got %v", expected, words)
	}
}

func TestSortWordsByA_EmptySlice(t *testing.T) {
	words := []string{}
	sortWordsByA(words)
	expected := []string{}
	if fmt.Sprint(words) != fmt.Sprint(expected) {
		t.Errorf("Expected %v, got %v", expected, words)
	}
}

func TestSortWordsByA_SameNumberOfAs(t *testing.T) {
	words := []string{"a", "aa", "aaa"}
	sortWordsByA(words)
	expected := []string{"aaa", "aa", "a"}
	if fmt.Sprint(words) != fmt.Sprint(expected) {
		t.Errorf("Expected %v, got %v", expected, words)
	}
}

func TestSortWordsByA_NoAsInAnyWord(t *testing.T) {
	words := []string{"b", "bb", "bbb"}
	sortWordsByA(words)
	expected := []string{"bbb", "bb", "b"}
	if fmt.Sprint(words) != fmt.Sprint(expected) {
		t.Errorf("Expected %v, got %v", expected, words)
	}
}
