# Problem Description

Write a function that sorts a bunch of words by the number of
character “a”s within the word (decreasing order). If some words
contain the same amount of character “a”s then you need to sort
those words by their lengths.

Input :
``` 
["aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"]
``` 
Output :
``` 
["aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd", "fdz", "ef", "kf", "zc", "l"]
``` 

# Problem Solution Approach

1. A struct named WordInfo is defined to hold the information of "a count" and "length" of a word and the word itself.
2. An WordInfo array is created with the same length of the input array.
3. Iterate over the input words and count "a" in every word and store the count and length in the WordInfo array.
4. Sort the WordInfo array by "a count" then sort the array by length.
5. Update the input array with the sorted words.


# How to run main file

``` 
go run main.go
```

# How to run test file

```
go test
```