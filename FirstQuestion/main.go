
package main

import (
	"fmt"
	"sort"
	"strings"
)

// WordInfo stores the word, count of 'a' in the word, and the length of the word
type WordInfo struct {
	word    string
	countOfA int
	length   int
}

// sortWordsByA sorts a slice of words based on the number of 'a's and then by length
func sortWordsByA(words []string) {
	// Precompute counts and lengths
	wordInfos := make([]WordInfo, len(words))
	for i, word := range words {
		wordInfos[i] = WordInfo{
			word:    word,
			countOfA: strings.Count(word, "a"),
			length:   len(word),
		}
	}

	// Sort using the precomputed information
	sort.Slice(wordInfos, func(i, j int) bool {
		// First, sort by the number of 'a's in descending order
		if wordInfos[i].countOfA != wordInfos[j].countOfA {
			return wordInfos[i].countOfA > wordInfos[j].countOfA
		}
		// If the number of 'a's is equal, sort by length in descending order
		return wordInfos[i].length > wordInfos[j].length
	})

	// Update the original slice
	for i, info := range wordInfos {
		words[i] = info.word
	}
}

func main() {
	words := []string{"aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"}
	sortWordsByA(words)
	fmt.Println(words)
}
